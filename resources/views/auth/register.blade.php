@extends('layouts.home')

@section('navcontent')
  <div class="flex justify-center mt-5">
    <div class="bg-white w-2/5 p-6 text-center rounded-md">
      <div><p class="font-bold text-2xl mb-3">REGISTRATION PAGE</p></div>
      <div>
        <form action="{{ route('register') }}" method="post">
          @csrf
          <div class="mb-2">
            <label for="name"  class="block font-semibold">Full Name</label>
            <input type="text" name="name" id="name" placeholder="Enter Full Name" value="{{ old('name') }}"
              class="border bg-gray-300 p-3 rounded-md w-full @error('name') border-red-500 @enderror"
            >
            <div>
              @error('name')
                <p class="text-red-500 text-sm">
                  {{ $message }}
                </p>
              @enderror
            </div>
          </div>
          <div class="mb-2">
            <label for="email" class="block font-semibold">Email</label>
            <input type="email" name="email" id="email" placeholder="Enter Email" value="{{ old('email') }}"
              class="border bg-gray-300 w-full p-3 rounded-md @error('email') border-red-500 @enderror"
            >
            <div>
              @error('email')
                <p class="text-red-500 text-sm">
                  {{ $message }}
                </p>
              @enderror
            </div>
          </div>
          <div class="mb-2">
            <label for="password" class="block font-semibold">Password</label>
            <input type="password" name="password" id="password" placeholder="Enter Password"
              class="border bg-gray-300 w-full p-3 rounded-md @error('password') border-red-500 @enderror"
            >

            <div>
              @error('password')
                <p class="text-red-500 text-sm">
                  {{ $message }}
                </p>
              @enderror
          </div>
          <div class="mb-2">
            <label for="password_confirmation" class="block font-semibold">Confirm Password</label>
            <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password"
              class="border bg-gray-300 w-full p-3 rounded-md @error('password_confirmation') border-red-500 @enderror"
            >
            <div>
              @error('password_confirmation')
                <p class="text-red-500 text-sm">
                  {{ $message }}
                </p>
              @enderror
          </div>
          <div>
            <button type="submit" class="bg-blue-600 text-white p-3 rounded-md w-full mt-3 font-semibold">REGISTER</button>
          </div>
        </form>
      </div>
    </div>
  </div>
@endsection