<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <title>Law Pavilion</title>
</head>
<body class="bg-gray-200">
  <nav class="flex justify-between px-5 bg-purple-300 w-full p-3">
    <ul class="flex font-bold">
      <a href="{{ route('home') }}">
        <li class="mr-3">Home</li>
      </a>
      @auth
        <a href="{{ route('dashboard') }}">
          <li>Dashboard</li>
        </a>
      @endauth
    </ul>

    <ul class='flex'>
      @guest
        <a href="{{ route('login') }}">
          <li class="mr-3">Login</li>
        </a>
        <a href="{{ route('register') }}">
          <li class="mr-3">Register</li>
        </a>
      @endguest
      
      @auth
        <form action="{{ route('logout') }}" method="post">
          @csrf
          <button type="submit">Logout</button>
        </form>
      @endauth
    </ul>
  </nav>

  @yield('navcontent')
</body>
</html>